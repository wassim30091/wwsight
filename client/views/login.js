Template.login.events({
    'submit #login': function(event) {
        event.preventDefault();
        var emailVar = event.target.loginEmail.value;
        var passwordVar = event.target.loginPassword.value;
        Meteor.loginWithPassword(emailVar, passwordVar, function(error){
        	if (error){
				 $("#errorMessage").toggleClass('hidden');				
        	}
        });

       
    }
});