Template.index.events({

    'submit #newJob': function(e){
        e.preventDefault();
        
        var author = $ ("input[name='auteur']").val();
        var title = $ ("input[name='titre']").val();
        var content = $ ("input[name='contenu']").val();
        var date = $ ("input[name='date']").val();
        var lieu = $ ("input[name='lieu']").val();

        
        var post = {
                author: author,
                title: title,
                description: content,
                date: date,
                lieu: lieu
        }
        console.log("New job submitted");
        Posts.insert(post, function(err, id){
		    if(err){
		        alert(err.reason)
		   

		    }

		});
	},

	'submit #edit_job': function(e) {
	    e.preventDefault();

	    var currentPostId = this._id;

	    var postProperties = {
	      author: $(e.target).find('[name=auteur]').val(),
	      title: $(e.target).find('[name=title]').val(),
	      description: $(e.target).find('[name=content]').val(),
	      date: $(e.target).find('[name=date]').val(),
	      lieu: $(e.target).find('[name=lieu]').val()
	    }
	   
	    Posts.update(currentPostId, {$set: postProperties}, function(error) {
	      if (error) {
	        console.log("Error edit post");
	        alert(error.reason);
	      } else {
	        Router.go('/');
	      }

	    });

	    document.getElementById(currentPostId).click();

	    
  	},

    'click .delete': function(e) {
	    e.preventDefault();

	    var currentPostId = this._id;

	    if (confirm("Supprimer ?")) {
	      Posts.remove({_id: currentPostId});

	 	}

    },

	/*'submit #newStaff' : function(e){
		e.preventDefault();

		var name = $ ("input[name='name']").val();
		var firstname = $ ("input[name='firstname']").val();
		var job = $ ("input[name='job']").val();
		var skills = $ ("input[name='skills']").val();
		var social = $ ("input[name='social']").val();
		var country = $ ("input[name='country']").val();
		var tags = $ ("input[name='tags']").val();

		var staff = {
			name: name,
			firstname: firstname,
			job: job,
			skills: skills,
			social: social,
			country: country,
			tags: tags
		}
			console.log("new member add to staff");
		Staff.insert(staff, function(err, id){
		    if(err){
		        alert(err.reason)
			}

    	});

	}
*/
});


Template.index.helpers({
  ownPost: function() {
    return this.userId === Meteor.userId();
  }

});
