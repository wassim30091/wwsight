Meteor.publish("allPostsHeaders", function(){
    return Posts.find({}, {
        fields: {content: 0}
    });
});

Meteor.publish("PostsHeaders", function(id){
	return Posts.find({_id: id}, {
		fields: {content: 0}
	});
});

/*Meteor.publish("allMembersHeaders", function(){
	return Staff.find({}, {

	});
});*/