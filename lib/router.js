Router.configure({
    layoutTemplate: 'mainLayout',
    notFoundTemplate: 'notFound'
});

Router.route('/', {
    name : "index",
    data : function(){
    	var posts = Posts.find();
      //var staff = Staff.find();

    		return {
    			posts: posts
          //staff: staff
    		};
    },
    waitOn: function(){
    	return Meteor.subscribe("allPostsHeaders");
    }

});

Router.route('/login',{
	name: "login",
  data : function(){
      if (Meteor.user()) {
        Router.go('/');

      }
  }
});


