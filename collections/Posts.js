Posts = new Mongo.Collection("Posts");


Posts.allow({

	update: function(userId, post) { return true;},
  	remove: function(userId, post) { return true;},
    insert: function(userId, post) { return true;}

});